package Mob;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import Core.Server.ServerFacade;
import World.Area;
import World.Grid;
import World.Location;
import World.WMap;


/*
 *  Mob.class
 *  Provides basic mob logic functions 
 */
public class Mob implements Location{
        
        
	private int mobID, uid;
	private MobData data;
	private MobController control;
	private List<Integer> iniPackets = new ArrayList<Integer>();
	private List<Integer> needIni = Collections.synchronizedList(new ArrayList<Integer>());
	private ServerFacade sface;
        
	/*
	 * Initializes the mob
	 * Params:
	 * mobID = type of mob in question
	 * id = unique ID of mob
	 * mdata = pointer to mobs data object 
	 * * cont = pointer to this mobs MobController object
	 */
        
	public Mob(int mobID, int id, MobData mdata, MobController cont) {
		super();
		this.uid = id;
		this.mobID = mobID;
		this.data = mdata;
		this.sface = ServerFacade.getInstance();
		this.data.setPath (  this.createPath(this.data.getWaypointCount() , this.data.getWaypointHop(), data.getSpawnx(), data.getSpawny()));
		this.data.setCurentWaypoint(0);
		this.control = cont;
		// this.joinGrid(this.data.getGridID());
		WMap.AddMob(id, this);
	}
        
	public int getMobID() {
		return mobID;
	}
	@Override
	public int getuid() {
		return this.uid;
	}
	@Override
	public void setuid(int uid) {
		this.uid = uid;
	}
	@Override
	public float getlastknownX() {
		return this.data.getX();
	}
	@Override
	public float getlastknownY() {
		return this.data.getY();
	}
	@Override
	public SocketChannel GetChannel() {
		return null;
	}
	@Override
    public short getState() {
		return 0;
	}
	// Join mob into the grid based proximity system 
	private void joinGrid(int grid){
		this.reset(false);
		Grid g = WMap.getGrid(grid);
		this.data.setMyGrid(g);
		if (!this.data.hasGrid()){ System.out.println("FUUUUUUUUUuuuuuuuuuuu!!!!!"); }
		else {
			Area a = this.data.getMyGrid().update(this);
			this.data.setMyArea(a);
			this.data.addAreaMember(this);
			//this.spawnage();
		}
	}
	// update our area
	private void updateArea() {
		Area a = this.data.getMyGrid().update(this);
		if (this.data.getMyArea() != a){
			this.data.getMyArea().moveTo(this, a);
			this.data.setMyArea(a);
			a.addMember(this);
		}
	}
	// remove mob from its current area
	private void rmAreaMember() {
		this.data.getMyArea().rmMember(this);
	}
	// perform actions needed to spawn the mob in the game world
	private void spawnage() {
		this.send(MobPackets.getInitialPacket(this.mobID, this.uid, this.data));
	}
	
	// return the waypoint number i
	private float[] getWaypoint(int i)
	{
	    return (float [])this.data.getWaypoint(i);
	}
	// return total ammount of waypoints generated for the mob
	private int waypointCount()
	{
	    return this.data.getPathSize();
	}
	/* generates the waypoint needed for the idle moving pattern
	 * Params:
	 * wpc = waypoints per each side
	 * hop = distance between waypoints
	 * x = starting x coordinate
	 * y = starting y coordinate
	 */
	private Map<Integer, Object> createPath(int wpc, float hop, float x, float y)
	{
	    Random generator = new Random();
	    Map<Integer, Object> path = Collections.synchronizedMap(new HashMap<Integer, Object>());
	    int rand = generator.nextInt(100);	
		float tmp[] = new float[]{x, y};
		float []pfft;
		// int counter = 0;
		int side = wpc;
		for (int a=0; a < wpc; a++)
		{
		  if ((rand % 2) == 0){ tmp[0] += hop;    }
		  else { tmp[0] -= hop; }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(a, pfft);
		  // System.out.println("["+a+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		for (int b=wpc; b < (wpc+side); b++)
		{
		  if ((rand % 2) == 0){ tmp[1] += hop;    }
		  else { tmp[1] -= hop; }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(b, pfft);
		  // System.out.println("["+b+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int c=wpc; c < (wpc+side); c++)
		{
		  if ((rand % 2) == 0){ tmp[0] -= hop;  }
		  else { tmp[0] += hop;   }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(c, pfft);
		  // System.out.println("["+c+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int d=wpc; d < (wpc+side); d++)
		{
		  if ((rand % 2) == 0){ tmp[1] -= hop; }
		  else { tmp[1] += hop;  }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(d, pfft);
		  // System.out.println("["+d+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		return path;
	  }
	// perform 1 action on the map based on the status of mob
	// return true if players are near, false otherwise
	public boolean run() {
		if (!this.data.hasGrid()){ 
			System.out.print("Mob " + this.uid + " Joining grid..");
			this.joinGrid(this.data.getGridID());
			System.out.println("done");
		}
		if (!this.needIni.isEmpty()) this.checkEnvironment();
		
		boolean hasPlayers = false;
		int i = this.data.getCurentWaypoint();
		float  []waypoint = new float[]{0,0};
		
		// check if mob is alive
		if (this.data.isAlive()){
			// System.out.println(this.uid + " is alive");
			// check if mob has gone too far from its initial spawn point
			if (WMap.distance(this.data.getX(), this.data.getY(), this.data.getSpawnx(), this.data.getSpawny()) > this.data.getMoveRange()){
				//System.out.println(this.uid + " is too far from spawn");
				this.reset(true);
			}
			// logic if mob has been aggroed
			else if(this.data.isAggro()){
				System.out.println(this.uid + " is aggroed by " + this.data.getAggroID());
				if (WMap.CharacterExists(this.data.getAggroID())){
						Location loc = WMap.getCharacter(this.data.getAggroID());
						// attack target and/or move towards it
						if (WMap.distance(this.data.getX(), this.data.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAttackRange()){
							// System.out.println(this.uid + " is attacking " + loc.getuid());
							this.attack(loc);
						}
						this.setLocation(loc.getlastknownX(), loc.getlastknownY());
						hasPlayers = true;
				}
				else {
					this.reset(true);
				}	
			}
			// mob hasn't been aggroed
			else {
				// move mob to it's next waypoint
				hasPlayers = this.checkAggro();
				if (hasPlayers){
					if (i >= this.waypointCount()) i=0;
				   
					waypoint = this.getWaypoint(i);
					// System.out.println(this.uid + " is moving to x:" + waypoint[0] + " y: " + waypoint[1]);
					this.setLocation(waypoint[0], waypoint[1]);
					i++;
					this.data.setCurentWaypoint(i);
				}
				
			}
			
		}
		// check if its time to respawn
		else if (System.currentTimeMillis() - this.data.getDied() > this.data.getRespawnTime()){
			this.data.setAlive(true);
			this.spawnage();
			
		}
		return hasPlayers;
		
	}
	// attack target loc
	private void attack(Location loc) {
		
	}
	// resets mobs data
	private void reset(boolean sendMove) {
		this.data.resetDamage();
		this.data.setHp(this.data.getMaxhp());
		this.data.setCurentWaypoint(0);
		this.data.setX(this.data.getSpawnx());
		this.data.setY(this.data.getSpawny());
		if (sendMove) this.send(MobPackets.getMovePacket(this.uid, this.data.getX(), this.data.getY()));
		this.data.setAggro(false);
		this.data.setAggroID(0);
                
	}
	// handle damages receiving
	public void recDamge(int uid, int dmg){
		if (this.data.hasPlayerDamage(uid)){
			int tmp = this.data.getPlayerDamage(uid);
			tmp += dmg;
			this.data.setDamage(uid, tmp);
		}
		else{
			this.data.setDamage(uid, dmg);
		}
                
		Map <Integer, Integer>mp = this.data.getDamage();
		synchronized(mp) {  //synchronized iteration for thread safe operations
			Iterator <Map.Entry<Integer, Integer>> it = mp.entrySet().iterator();
			int key;
			int value = 0;
			int hiDmg = 0;
			int hiID = 0;
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pairs = it.next();
				key = pairs.getKey();
				value = pairs.getValue();
				if (value > hiDmg){
					hiDmg = value;
					hiID = key;
				}
			}
			this.data.setAggro(true);
			this.data.setAggroID(hiID);
		}               

		this.data.reduceHp(dmg);
		if (this.data.getHp() <= 0) this.die();
	}
	// perform actions needed to finalize mob's death
	private void die(){
		this.rmAreaMember();
		this.data.setDied(System.currentTimeMillis());
		this.data.setAlive(false);
		this.send(MobPackets.getDeathPacket());
		this.reset(false);
	}
	// check if mob is close enough to player to aggro it
	private boolean checkAggro() {
		boolean hasPlayers = false;
        Map<Integer, Location> mp = new HashMap<Integer, Location>();
        Location loc;
        synchronized(mp){
            mp.putAll(this.data.getMyArea().nearMap());
        	Iterator<Map.Entry<Integer, Location>> iter = mp.entrySet().iterator();
        	while(iter.hasNext()) {
       			Map.Entry<Integer, Location> pairs = iter.next();
       			loc = pairs.getValue();
       			if (loc.GetChannel() != null){
       				hasPlayers = true;
       				if (WMap.distance(this.data.getX(), this.data.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAggroRange()){
       					this.data.setAggro(true);
       					this.data.setAggroID(loc.getuid());
       					break;
       				}
                                         
       			}
       		}
        }
        return hasPlayers;
    }

	// send packet to all nearby players
	private void send(ByteBuffer buf) {		
		this.data.getMyArea().sendToMembers(this.getuid(), buf);
	}
	// set mobs location on the map and send move packet to players
	private void setLocation(float x, float y) {
		this.data.setX(x);
		this.data.setY(y);
		this.updateArea();
		ByteBuffer buf = MobPackets.getMovePacket(this.uid, this.data.getX(), this.data.getY());
		this.send(buf);
	}
	// return reference to this mob's controller
	public MobController getControl() {
		return control;
	}
	private ByteBuffer getInitPacket(){
		return MobPackets.getInitialPacket(this.mobID, this.uid, this.data);
	}
	// update near by objects, called by area
	public void updateEnvironment(List<Integer> players){
		this.needIni.addAll(players);
	}
	// check if mob needs to send initial packet to players
	public void checkEnvironment() {
		Iterator<Integer> plIter = this.iniPackets.iterator();
		Integer tmp = null;
		while(plIter.hasNext()) {
			tmp = plIter.next();
			
			if (!WMap.CharacterExists(tmp)){
				this.needIni.remove(tmp);
			} else if(!this.needIni.contains(tmp)) { //if character is no longer in range
				plIter.remove(); //remove it
			} else{
				this.needIni.remove(tmp);
			}
		}
		this.sendInit();
	}
	// send initial packet to everyone in needIni list
	private void sendInit() {
		synchronized(this.needIni){
			Iterator<Integer> siter = this.needIni.iterator();
			Integer tmp = null;
			while(siter.hasNext()) {
				tmp = siter.next();
				if (WMap.CharacterExists(tmp)){
					SocketChannel sc = WMap.getCharacter(tmp).GetChannel();
					//this.log.severe(this, "init for " + tmp);
					ByteBuffer buf = this.getInitPacket();
					//this.log.severe(this, "got packet");
					// prevent the mob from crashing because there aren't initial packet yet
					if (buf != null){
						//this.sface.getConnectionByChannel(sc).addWrite(buf);
						this.sface.addDelayedWriteByChannel(sc, buf);
					}
				}
				else {
					siter.remove();
				}
			}
			this.iniPackets.addAll(this.needIni);
			this.needIni.clear();
		}
	}
        
}
