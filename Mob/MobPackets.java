package Mob;

import java.nio.ByteBuffer;

import Tools.BitTools;

/*
 * MobPackets.class
 * Generates the necessary packets for the mobs
 */

public class MobPackets {
	
	public static ByteBuffer getMovePacket(int uid, float x, float y){
		byte[] moveBucket = new byte[48];
		ByteBuffer retBuff = ByteBuffer.allocate(moveBucket.length);
		byte[] uniqueID = BitTools.intToByteArray(uid);
		byte[] moveX = BitTools.floatToByteArray(x);
		byte[] moveY = BitTools.floatToByteArray(y);
		
		moveBucket[0] = (byte)moveBucket.length;
		moveBucket[4] = (byte)0x05;
		moveBucket[6] = (byte)0x0D;
		moveBucket[8] =  (byte)0x02;
		moveBucket[9] =  (byte)0x10;
		moveBucket[10] = (byte)0xa0; 
		moveBucket[11] = (byte)0x36;

		
		for(int i=0;i<4;i++) {
			moveBucket[i+12] = uniqueID[i];
			moveBucket[i+20] = moveX[i];
			moveBucket[i+24] = moveY[i];
			moveBucket[i+28] = moveX[i];
			moveBucket[i+32] = moveY[i];
		}
		
		retBuff = ByteBuffer.wrap(moveBucket);
		
		return retBuff;
	}
	public static ByteBuffer getDeathPacket() {
		// TODO Auto-generated method stub
		return null;
	}
	//public static ByteBuffer getInitialPacket(int mobID, int uid, float x, float y) {
	public static ByteBuffer getInitialPacket(int mobID, int uid, MobData mdata) {
        byte[] mobBucket = new byte[608];
        byte[] size = BitTools.shortToByteArray((short)mobBucket.length);
        ByteBuffer retBuff = ByteBuffer.allocate(mobBucket.length);
       
        byte[] mobid = BitTools.shortToByteArray((short)mobID);
        byte[] mobUid = BitTools.intToByteArray(uid);
        byte[] xCoords = BitTools.floatToByteArray(mdata.getX());
        byte[] yCoords = BitTools.floatToByteArray(mdata.getY());
        byte[] hp = BitTools.intToByteArray(mdata.getMaxhp());
        
        for(int i=0;i<2;i++) {
                mobBucket[i] = size[i];
                mobBucket[i+64] = mobid[i];
        }
        for(int i=0;i<4;i++) {
                mobBucket[i+12] = mobUid[i];
                mobBucket[i+84] = xCoords[i];
                mobBucket[i+88] = yCoords[i];
                mobBucket[i+72] = hp[i];
        }
       
        mobBucket[4] = (byte)0x05;
        mobBucket[6] = (byte)0x03;
        mobBucket[8] = (byte)0x02;
        
        
       
        retBuff = ByteBuffer.wrap(mobBucket);
       
        return retBuff;
}

}
