package Mob;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import logging.ServerLogger;

import Database.MobDAO;

public class MobController implements Runnable {
	private Map<Integer, Mob> mobs = Collections.synchronizedMap(new HashMap<Integer, Mob>());
	private int mobID, mobCount, uidPool;
	private int map, spawnx, spawny, spawnRadius,wpCount,wpHop, respawnTime;
	private boolean active;
	private ServerLogger log = ServerLogger.getInstance();
	
	
	public MobController(int ID, int Count, int Pool, int []data) {
		super();
		this.mobID = ID;
		this.mobCount = Count;
		this.uidPool = Pool;
		this.map = data[0];
		this.spawnx = data[1];
		this.spawny = data[2];
		this.spawnRadius = data[3];
		this.wpCount = data[4];
		this.wpHop = data[5];
		this.respawnTime = data[6];
		this.setActive(false);
		
		this.init();
	}
	private void init(){
		MobData template = MobDAO.getMobData(mobID);
		template.setGridID(map);
		template.setWaypointCount(wpCount);
		template.setWaypointHop(wpHop);
		template.setRespawnTime(respawnTime);
		Random r = new Random();
		MobData tmp= null;
		Mob mob = null;
		int uid = uidPool;
		int x,y;
		this.log.info(this, "Creating mob objects ");
		for (int i=0; i < this.mobCount; i++){
			tmp = template.copy();
			// randomize spawn coordinates 
			x = this.spawnx + r.nextInt(2*this.spawnRadius) - this.spawnRadius;
			y = this.spawny + r.nextInt(2*this.spawnRadius) - this.spawnRadius;
			tmp.setSpawnx(x);
			tmp.setSpawny(y);
			mob = new Mob(this.mobID, uid, tmp, this);
			uid++;
			mobs.put(i, mob);
		}
		this.log.info(this, this.mobCount + " mobs succesfully created");
	}
	public boolean isActive() {
		return active;
	}
	private void setActive(boolean active) {
		this.active = active;
	}
	public synchronized void run(){
		if (!this.active){
			this.setActive(true);
			this.log.info(this, "Controller active in thread " + Thread.currentThread());
			boolean hasPlayers;
		
			while(this.active){
				hasPlayers = false;
				synchronized(this.mobs){
					Iterator<Map.Entry<Integer, Mob>> iter = this.mobs.entrySet().iterator();
					while(iter.hasNext()) {
						Map.Entry<Integer, Mob> pairs = iter.next();
						if (hasPlayers) pairs.getValue().run();
						else hasPlayers = pairs.getValue().run(); 
					}
				}
				try {
					if (!hasPlayers) this.setActive(false);
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
			}
			this.log.info(this, "Controller deactivated in thread " + Thread.currentThread());
		}
		
	}
	
	

}
