package Core.Server.Core;


import java.io.IOException;
import java.nio.channels.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;


import Connections.Connections;
import Core.ThreadPools.PacketHandlerPool;
import Core.PacketHandler.PacketHandler;
import Core.QueueHandlers.ReadHandler;
import Core.QueueHandlers.WriteHandler;
import logging.ServerLogger;



public class ServerCore implements Runnable{
	private Selector selector;
	private ServerSocket sSocket;
	private PacketHandlerPool php; 
	public static boolean running = true;
	private ServerLogger logging;
	
	private int readQsize = 100; //default read/write queue size for each
	private int writeQsize = 100; //new connection. can be adjusted while running(applies to new connections)
	
	private Connections con; 
	private PacketHandler phand;
	private InetSocketAddress bindi;
	
	public static volatile LinkedBlockingQueue<SocketChannel> finalizeList = new LinkedBlockingQueue<SocketChannel>();
	
	
	public ServerCore(InetSocketAddress bind, PacketHandlerPool ppool, Connections cons, PacketHandler ph) {	
		this.php = ppool;
		this.phand = ph;
		this.bindi = bind;
		this.con = cons;
		this.logging = ServerLogger.getInstance();
		new Thread(this).start();
	}
	
	//method for initializing listening socket
	private void init(InetSocketAddress bind, Connections con) {		
		try {
			ServerSocketChannel ssChannel = ServerSocketChannel.open(); 
			ssChannel.configureBlocking(false); //set as non-blocking
			this.selector = Selector.open();
			ssChannel.register(this.selector, SelectionKey.OP_ACCEPT); //set ops to accept connections		
			this.sSocket = ssChannel.socket(); //get socket	
			this.sSocket.bind(bind); //bind to ip/port
			Connections.setSelector(this.selector);
			this.con = con;
			
			this.logging.logMessage(Level.INFO, this, "New listening socket at port: " + this.sSocket.getLocalPort());
			
		} catch (ClosedChannelException e) {
			this.logging.logMessage(Level.SEVERE, this, e.getMessage());
		} catch (IOException e) {
			this.logging.logMessage(Level.SEVERE, this, e.getMessage());
		}
		this.makeItSo();
	}
	
	//captain Jean Luc Picard
	private void makeItSo() {
		while(running) {
			try {
				this.selector.select(100); //block for 100ms at most and select all channels ready for operations
			} catch (IOException e) {
				this.logging.logMessage(Level.SEVERE, this, e.getMessage());
			}
			Set<SelectionKey> readySet = this.selector.selectedKeys();
			
			
			synchronized(finalizeList) {
				while(!finalizeList.isEmpty()) {
					SocketChannel tmp = finalizeList.poll();
					this.con.removeConnection(tmp);
					try {
						this.logging.logMessage(Level.INFO, this, "Closing connection: " + tmp);
						if(tmp.isConnected()) {
							tmp.close();
						}
						tmp.keyFor(this.selector).cancel();
						readySet.remove(tmp.keyFor(selector));
					} catch (IOException e) {
						this.logging.logMessage(Level.WARNING, this, e.getMessage());
					}
				}
			}
			
			CountDownLatch threadSync = new CountDownLatch(readySet.size());
			Iterator<SelectionKey> keySetIter = readySet.iterator();
			
			
			//Iterate all selected keys
			while(keySetIter.hasNext()) {
				SelectionKey key = keySetIter.next();
				
				//always a good idea to check validity of dem keys when dealin' with em
				//a key is valid if it's channel is open and the key's not canceled
				if(key.isValid()) {
					
					//channel is ready to accept new connection
					if(key.isAcceptable()) {
						try {
							ServerSocketChannel server = (ServerSocketChannel)key.channel();
							SocketChannel client = server.accept();	
							client.configureBlocking(false); //set as non-blocking
							client.socket().setSendBufferSize(client.socket().getSendBufferSize()*10);
							System.out.println("Client writebuffer size: " + client.socket().getSendBufferSize());
							client.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE); //set valid ops for the channel as read/write
							
							this.phand.newConnection(this.con.addAndReturnConnection(client, this.readQsize, this.writeQsize));
							this.logging.logMessage(Level.INFO, this, "New connection: " + client);
						} catch (ClosedChannelException e) {
							this.logging.logMessage(Level.WARNING, this, e.getMessage());
						} catch (IOException e) {
							this.logging.logMessage(Level.WARNING, this, e.getMessage());
						}
					} else 
					//ready for reading	
					if(key.isReadable()) {
						SocketChannel tmp = (SocketChannel)key.channel();
						if(this.con.isChannelRegistered(tmp)) {
							this.php.executeProcess(new ReadHandler(tmp, this.con, this.phand, threadSync));
						}
					} else
					//ready for writing
					if(key.isWritable()) {
						SocketChannel tmp = (SocketChannel)key.channel();
						if(this.con.isChannelRegistered(tmp)) {
							this.php.executeProcess(new WriteHandler(tmp, this.con, threadSync));
						}	
					}
						
				}
				
				keySetIter.remove();
				
			}
			
			try {
				threadSync.await(300, TimeUnit.MILLISECONDS); //wait for all dispatched operations to complete, or carry on if timeout is reached.
			} catch (InterruptedException e) {}
		}
	}
	
	//set as false to exit main loop
	public static void setRunning(boolean val) {
		running = val;
	}

	public Selector getSelector() {
		return selector;
	}

	public ServerSocket getsSocket() {
		return sSocket;
	}

	public static boolean isRunning() {
		return running;
	}


	public static LinkedBlockingQueue<SocketChannel> getFinalizeList() {
		return finalizeList;
	}
	
	public static synchronized void finalizeConnection(SocketChannel chan) {
		try {
			if(!finalizeList.contains(chan) && chan.isConnected()) {
				finalizeList.put(chan);
			}
		} catch (InterruptedException e) {}
	}

	public int getReadQsize() {
		return readQsize;
	}

	public void setReadQsize(int readQsize) {
		this.readQsize = readQsize;
	}

	public int getWriteQsize() {
		return writeQsize;
	}

	public void setWriteQsize(int writeQsize) {
		this.writeQsize = writeQsize;
	}

	public PacketHandler getPacketHandler() {
		return phand;
	}

	public void setPacketHandler(PacketHandler phand) {
		this.phand = phand;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		this.init(bindi, con);
	}


}

