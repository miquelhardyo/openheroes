package Core.QueueHandlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

import Connections.Connections;
import Core.PacketHandler.PacketHandler;
import Core.Server.Core.ServerCore;
import logging.ServerLogger;




public class ReadHandler implements Runnable {
	private Connections con;
	private SocketChannel cn;
	private PacketHandler han;
	private CountDownLatch cdl;
	private ServerLogger logging = ServerLogger.getInstance();
	
	public ReadHandler(SocketChannel sc, Connections cn, PacketHandler ph, CountDownLatch count) {
		this.cn = sc;
		this.con = cn;
		this.han = ph;
		this.cdl = count;
	}
	
	public void run() {
			SocketChannel current = this.cn;
		if(this.con.isChannelRegistered(current)) {
				LinkedBlockingQueue<ByteBuffer> readBuffer = this.con.getConnection(current).getReadBuffer();
				if(!readBuffer.equals(null)) {
					int numRead=0;			
					ByteBuffer buff = ByteBuffer.allocate(1024);
				
					try {
						numRead = current.read(buff);	
						if (numRead == -1) { //if client has disconnected, finalize the key/channel/thread
							try {
								ServerCore.finalizeList.put(current);
							} catch (InterruptedException e) {
								this.logging.logMessage(Level.WARNING, this, "Thread interrupted while attempting to finalize connection");
							}				
						} else {
							buff.flip();
							try {
								if(buff.remaining() > 0) {
									readBuffer.put(buff);
									this.han.processList(this.con.getConnection(current));
								}
							} catch (InterruptedException e) {
								this.logging.logMessage(Level.WARNING, this, "Thread interrupted while attempting to dispatch operation");
							} 
						}
					} catch (IOException e) {
						this.logging.logMessage(Level.WARNING, this, "Error while reading socket");
						ServerCore.finalizeConnection(this.cn);
					} catch (NullPointerException np) {
						this.logging.logMessage(Level.WARNING, this, "Nullpointer while attempting to read socket. This should not be happening.");
						ServerCore.finalizeConnection(this.cn);
					}										
		
				}
		}
		this.cdl.countDown(); //countdown decrement
	}
}
