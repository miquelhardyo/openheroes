package chat.chatCommandHandlers;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;


import World.WMap;
import Player.Character;
import Connections.Connection;
import Core.Server.ServerFacade;
import chat.ChatCommandExecutor;

public class GMChat implements ChatCommandExecutor {


	public void execute(String[] parameters, Connection source) {
		System.out.println("Handling GM red chat command");
		byte[] gmsg = new byte[14+parameters[0].length()];
		byte[] msg = parameters[0].getBytes();
		
		gmsg[0] = (byte)gmsg.length;
		gmsg[4] = (byte)0x03;
		gmsg[6] = (byte)0x50;
		gmsg[7] = (byte)0xC3;
		gmsg[8] = (byte)0x01;
		gmsg[9] = (byte)0x23;
		
		for(int i=0;i<msg.length;i++) {
			gmsg[i+13] = msg[i];
		}
		
		Iterator<Map.Entry<Integer, Character>> iter = WMap.getCharacterMap().entrySet().iterator();
		Character tmp;
		while(iter.hasNext()) {
			Map.Entry<Integer, Character> pairs = iter.next();
			tmp = pairs.getValue();
			ServerFacade.getInstance().addDelayedWriteByChannel(tmp.GetChannel(), ByteBuffer.wrap(gmsg));
		}
		
	}

}
