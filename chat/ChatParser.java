package chat;

import java.util.HashMap;
import java.util.Map;

import Connections.Connection;

import chat.chatCommandHandlers.*;

public class ChatParser {
	private static volatile ChatParser instance = null;
	private final String cmdDelimiter = "!";
	private final String paramDelimiter = ":";
	private Map<String, ChatCommandExecutor> commandList = new HashMap<String, ChatCommandExecutor>();
	
	private ChatParser() {
		this.commandList.put("item", new ItemSpawner());
		this.commandList.put("announce", new GMChat());
		this.commandList.put("heal", new HealCommand());
	}
	
	public static synchronized ChatParser getInstance(){
		if (instance == null){
			instance = new ChatParser();
		}
		return instance;
	}

	public String getCommandDelimiter() {
		return cmdDelimiter;
	}
	
	public String getParameterDelimiter() {
		return this.paramDelimiter;
	}

	public Map<String, ChatCommandExecutor> getCommandList() {
		return commandList;
	}

	public boolean parseAndExecuteChatCommand(String msg, Connection con) {
		System.out.println("Parsing a chat command: " + msg);
		String[] commands = msg.split(this.cmdDelimiter);
		for(int i=0;i<commands.length;i++) {
			String[] splat = commands[i].split(paramDelimiter);
			if(this.commandList.containsKey(splat[0])) {
				String[] params = new String[splat.length-1];
				for(int ri=1;ri<splat.length;ri++) {
					params[ri-1] = splat[ri]; 
				}
				this.commandList.get(splat[0]).execute(params, con);
			}
		}
		return true;
	}
	
}
