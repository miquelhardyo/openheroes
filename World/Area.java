package World;

import java.util.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;

import Connections.Connection;
import Core.Server.ServerFacade;
import Mob.MobController;


// Area.class
// Keeps track of players within small area in side the game
// Each Area are members of some Grid, but Area is unaware of which
// Area is always a square as is its Grid

public class Area
{
  private Map<Integer, Location> members = Collections.synchronizedMap(new HashMap<Integer, Location>());
  private Map<Integer, Location> near = Collections.synchronizedMap(new HashMap<Integer, Location>());  
  private List<SocketChannel> playerList = Collections.synchronizedList(new ArrayList<SocketChannel>());
  
  private int gridx,gridy;
  private int uid;
  private int zone;
  private Grid grid;
  
  /** initializes Area
   parameter descriptions:
   gx, gy  -  (x,y) coordinates of the area within the grid that it belongs to
   mygrid  -  Pointer to the grid object
  */
  public Area(int gx, int gy, Grid mygrid)
  {
        this.gridx = gx;
        this.gridy = gy;
        this.grid = mygrid;
        this.zone = 0;
  }
  // adds new member (player, mob, npc, dropped item) to the Area
  public void addMember(Location ob)
  {
    // System.out.println("Item added to area " + this.uid);
    this.members.put(ob.getuid(), ob);
	if (ob.GetChannel() != null) { 
	 this.playerList.add(ob.GetChannel());
	}
	this.updateNear(true, ob);
  }
  // add new near member to list
  protected void addNear(Location loc){
	  if (!this.near.containsValue(loc)){
          this.near.put(loc.getuid(), loc);
          this.notifyMembers();
	  }
  }
  // remove member from near list
  protected void rmNear(Location loc){
	  if (near.containsValue(loc)){
		  this.near.remove(loc.getuid());
		  this.notifyMembers();
      }
  }
  public void setuid(int id)
  {
    this.uid = id;
  }
  public int getuid()
  {
    return this.uid;
  }
  private void updateNear(boolean add, Location loc){
	  int area[] = new int[]{this.gridx -1, this.gridy -1};
		Area tmp = null;
		for (int a=0; a <3; a++){
			for (int b=0; b <3; b++){
				if (this.grid.areaExists(area)){
					tmp = this.grid.getArea(area);
					if (add)tmp.addNear(loc);
					else tmp.rmNear(loc);
					//tmp.notifyMembers();
					area[1]++;
				}
			}
			area[1] = this.gridy -1;
			area[0]++;
		}
	this.notifyMembers();
  }
  // removes existing member from Area
  public void rmMember(Location loc)
  {
    System.out.println("Item removed from area: " + this.uid);
	if(members.containsKey(loc.getuid())) {
		members.remove(loc.getuid());
		this.updateNear(false, loc);
	}
  }
  // returns list containing all the players in this Area
  public List<SocketChannel> returnPlayerList() {
        return this.playerList;
  }
  // returns true if there are players in area, false if not
  public boolean hasPlayers()
  {
    return !playerList.isEmpty();
  }
  // returns true if Area has any members at all, false if not
  public boolean hasMembers()
  {
    return !this.members.isEmpty();
  }
  public Location getMember(int uid)
  {
    return (Location)members.get(uid);
  }
  // returns member map
  public Map<Integer, Location> getMemberMap()
  {
    return members;
  }
  public int[] getcoords()
  {
    int [] tmp = new int []{this.gridx, this.gridy};
    return tmp;
  }
  // return true if obj is member of this Area, else returns false
  public boolean isMember(Location obj)
  {
    return members.containsValue(obj);
  }
  // returns true if member whose uid is id is member of this Area, else returns false
  public boolean isMember(int id)
  {
    return members.containsKey(id);
  }
  // send packet buf to all members except one specified by uid
  public void sendToMembers(int uid, ByteBuffer buf)
  {
          SocketChannel tmp;
          Location loc;
          synchronized(this.near)
          {
                  Iterator<Map.Entry<Integer, Location>> iter = this.near.entrySet().iterator();
                  while(iter.hasNext()) {
                          Map.Entry<Integer, Location> pairs = iter.next();
                          loc = pairs.getValue();
                          tmp = loc.GetChannel();
                          
                          if (loc.getuid() != uid && tmp != null){
                              // write buf to player loc
                        	  Connection tmpc = ServerFacade.getInstance().getConnectionByChannel(tmp);
                              	if(tmpc != null) { 
                              		if(tmpc.addDelayedWrite(buf)) {
                              			System.out.println("Player: " + uid + " successfully added stuff in player: " + pairs.getKey() + " write queue");
                              		} else {
                              			System.out.println("Player: " + uid + " FAILED TO ADD stuff in player: " + pairs.getKey() + " write queue");
                              		}
                              	}
                          }
                  }
                  
          }
  }
  
  // notify all nearby objects of a change in members
  public void notifyMembers() {
	  synchronized(this.members){
		  MobController cont;
		  Iterator<Map.Entry<Integer, Location>> iter = this.members.entrySet().iterator();
		  Location loc = null;
		  while(iter.hasNext()) {
		  	  List<Integer> linteger = new ArrayList<Integer>();
		  	  linteger.addAll(this.near.keySet());

			  Map.Entry<Integer, Location> pairs = iter.next();
			  loc = pairs.getValue();
			  loc.updateEnvironment(linteger);
			  if (WMap.mobExists(loc.getuid())){
				  cont = WMap.GetMobController(pairs.getKey());
				  if (!cont.isActive()) this.grid.getThreadPool().executeProcess(cont);
			  }
		  }
	  }
  }
  public Map<Integer, Location> nearMap() {
	  return this.near;
  }
  public int getZone() {
	return this.zone;
  }
  public void setZone(int val) {
	  this.zone = val;
  }
  public void moveTo(Location loc, Area t) {
	if (this.members.containsValue(loc)){
		int coords[] = t.getcoords();
		int area[] = new int[]{this.gridx -1, this.gridy -1};
		Area tmp = null;
		for (int a=0; a <3; a++){
			for (int b=0; b <3; b++){
				if (this.grid.areaExists(area)){
					tmp = this.grid.getArea(area);
					if (WMap.distance(coords[0], area[0]) > 1){
						tmp.rmNear(loc);
					}
					if (WMap.distance(coords[1], area[1]) > 1){
						tmp.rmNear(loc);
					}
					area[1]++;
				}
			}
			area[1] = this.gridy -1;
			area[0]++;
		}
		this.members.remove(loc.getuid());
	}
	
  }
  
}
