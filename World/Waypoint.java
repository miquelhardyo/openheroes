package World;

public class Waypoint {
	private float x,y;
	
	public Waypoint(float tx, float ty){
		this.x = tx;
		this.y = ty;
	}
	public float getX(){
		return this.x;
	}
	public float getY(){
		return this.y;
	}
	

}
