package GameServer.GamePackets;

import java.nio.ByteBuffer;

import Connections.Connection;
import Encryption.Decryptor;
import Player.PlayerConnection;
import Player.Character;
import Tools.BitTools;

public class Equip implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}


	public ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con) {
		System.out.println("Handling equip");
		byte[] decrypted = new byte[(buffyTheVampireSlayer.get(0) & 0xFF)-8];
		
		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer.get(i+8) & 0xFF);
		}
		
		decrypted = Decryptor.Decrypt(decrypted);
		byte[] eq = new byte[24];
		Character cur = ((PlayerConnection)con).getActiveCharacter();
		byte[] cid = BitTools.intToByteArray(cur.getCharID());
		
		eq[0] = (byte)eq.length;
		eq[4] = (byte)0x04;
		eq[6] = (byte)0x0C;
		
		eq[8] = (byte)0x01;
		eq[9] = (byte)0xFF;
		eq[10] = (byte)0x14;
		eq[11] = (byte)0x08;
		eq[16] = (byte)0x01;
		eq[18] = (byte)0x01;
		
		for(int i=0;i<4;i++) {
			eq[12+i] = cid[i];
		}
		
		eq[19] = decrypted[1];
		eq[20] = decrypted[2];
		
		return ByteBuffer.wrap(eq);
	}

}
