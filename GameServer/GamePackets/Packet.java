package GameServer.GamePackets;

import java.nio.ByteBuffer;

import Connections.Connection;

public interface Packet {
	void execute(ByteBuffer buff);
	ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con);
}
