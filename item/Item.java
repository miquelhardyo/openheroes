package item;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import Tools.BitTools;
/*
 * TODO: uhhh.. lots of stuff
 */
public class Item {
	private int id;
	private int width, height;
	private boolean consumeable;
	private boolean permanent;
	private int npcPrice;
	private long expirationTime;
	private int minLvl, MaxLvl;
	private int type;
	
	public static final int EQUIPMENT = 1;
	public static final int POTION = 2;
	public static final int TELEPORT = 3;
	public static final int UPGRADE = 4;
	public static final int MANUAL = 5;
	public static final int MATERIAL = 6;
	
	public static int inc = 0;
	public static Map<Integer, Integer> iteMap = new HashMap<Integer, Integer>();
	
	public Item(){
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isConsumeable() {
		return consumeable;
	}

	public void setConsumeable(boolean consumeable) {
		this.consumeable = consumeable;
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public int getNpcPrice() {
		return npcPrice;
	}

	public void setNpcPrice(int npcPrice) {
		this.npcPrice = npcPrice;
	}

	public long getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(long expirationTime) {
		this.expirationTime = expirationTime;
	}

	public int getMinLvl() {
		return minLvl;
	}

	public void setMinLvl(int minLvl) {
		this.minLvl = minLvl;
	}

	public int getMaxLvl() {
		return MaxLvl;
	}

	public void setMaxLvl(int maxLvl) {
		MaxLvl = maxLvl;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public static ByteBuffer itemSpawnPacket(int charID, int itemID, float x, float y) {
		inc++;
		byte[] item = new byte[56];
		byte[] spawnX = BitTools.floatToByteArray(x);
		byte[] spawnY = BitTools.floatToByteArray(y);
		byte[] itid = BitTools.intToByteArray(itemID);
		byte[] chid = BitTools.intToByteArray(inc);
		
		item[0] = (byte)item.length;
		item[4] = (byte)0x05;
		item[6] = (byte)0x0e;
		
		for(int i=0;i<4;i++) {
			item[36+i] = spawnX[i];
			item[40+i] = spawnY[i];
			item[20+i] = itid[i];
			item[32+i] = chid[i]; 
		}
		
		iteMap.put(Integer.valueOf(inc), Integer.valueOf(itemID));
		
		return ByteBuffer.wrap(item);
	}

}
