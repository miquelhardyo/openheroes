package Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import logging.ServerLogger;

import Player.Player;


public class AccountDAO {
	private static ServerLogger log = ServerLogger.getInstance();
	/*
	 * Use this for authentication
	 * Return: Player instance if auth successful, null if failed
	 */
	public static Player authenticate(String username, String pass) {
		try {
			ResultSet rs = Queries.auth(new SQLconnection().getConnection(), username, pass).executeQuery();
			if(rs.next()) {
				return new Player(rs.getInt(1));
			} else {
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.severe(AccountDAO.class, "Database error: " + e.getMessage());
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.severe(AccountDAO.class, e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}
